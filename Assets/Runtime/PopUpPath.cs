public class PopUpPath 
{
	public const string POP_UP_UIHOME = "PopUp/UIHome";
	public const string POP_UP_UIPANEL = "PopUp/UIPanel";
	public const string POP_UP_UIPANEL_1 = "PopUp/UIPanel 1";
	public const string POP_UP_UIPANEL_2 = "PopUp/UIPanel 2";
	public const string POP_UP_UIPANEL_3 = "PopUp/UIPanel 3";
	public const string POP_UP_UIPANEL_4 = "PopUp/UIPanel 4";
	public const string POP_UP_UIPANEL_5 = "PopUp/UIPanel 5";
	public const string POP_UP_UIPANEL_6 = "PopUp/UIPanel 6";
	public const string POP_UP_UIPANEL_7 = "PopUp/UIPanel 7";
}