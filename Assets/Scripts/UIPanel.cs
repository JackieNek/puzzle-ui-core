﻿using Puzzle.UI;
using UnityEngine;
using UnityEngine.UI;

public class UIPanel : MonoBehaviour, IPopUpContent
{
    [SerializeField] private Text text;

    public string Text
    {
        set => text.text = value;
    }
}