using DG.Tweening;
using Puzzle.UI;
using UnityEngine;

public class StartPopUp : MonoBehaviour
{
    private void Awake()
    {
        DOTween.Init();
    }

    private void Start()
    {
        var popUp = Hub.Get<UIHome>(PopUpPath.POP_UP_UIHOME);
        Hub.Show(popUp.gameObject).Play();
    }
}


