using DG.Tweening;
using Puzzle.UI;
using UnityEngine;

public class UIHome : MonoBehaviour, IPopUpContent
{

    public void OnShowPanel()
    {
        var panel = Hub.Get<UIPanel>(PopUpPath.POP_UP_UIPANEL);
        panel.Text = "panel from home";
        PopUp.Stack.Push(panel.gameObject);
        Hub.Show(panel.gameObject).Play();
    }

}